package com.examplertf;

public class Person {

	private int _id;
	private String _name;
	private String _lastName;


	public int Id()
	{
		return _id;
	}
	
	public String Name()
	{
		return _name;
	}
	
	public String LastName()
	{
		return _lastName;
	}
	
	public void setId(int id)
	{
		this._id = id;
	}
	
	public void setName(String name)
	{
		this._name = name;
	}
	
	public void setLastName(String lastName)
	{
		this._lastName = lastName;
	}

	@Override
	public String toString(){
		return _name + _lastName;
	}
	
}
