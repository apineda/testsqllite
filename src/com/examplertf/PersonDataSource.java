package com.examplertf;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;



public class PersonDataSource implements IPersonDataSource {

	private SQLiteDatabase _db;
	private SqlHelper dbHelper;
	
	public PersonDataSource(Context context)
	{
		dbHelper = new SqlHelper(context);
		Log.d("PersonDataSource()", "Dbhelper Instance Created");
	}
	
	@Override
	public void open() throws SQLException {
		_db = dbHelper.getWritableDatabase();
		Log.d("PersonDataSource", "Connection Opened");
	}

	@Override
	public void close() {
		dbHelper.close();
	}

	@Override
	public Person addNew(Person person) {
		Person _person = null;
		ContentValues values = new ContentValues();
		values.put(Persons.COLUMN_NAME, person.Name());
		values.put(Persons.COLUMN_LASTNAME, person.LastName());
		
		long insertId = _db.insert(Persons.TABLE_PERSONS, null, values);
		
		Cursor cursor = _db.query(Persons.TABLE_PERSONS, 
								  Persons.getAllColumns(),
								  Persons.COLUMN_ID + "="  + insertId,
								  null,
								  null,
								  null,
								  null);
		
		if (cursor.moveToFirst()){
			_person = MapCursorToEntity(cursor);
		}
		
		cursor.close();
		
		return _person;
	}

	
	private Person MapCursorToEntity(Cursor cursor)
	{
		Person _person = new Person();
		_person.setId(cursor.getInt(0));
		_person.setName(cursor.getString(1));
		_person.setLastName(cursor.getString(2));
		
		return _person;
	}
	
	@Override
	public void delete(int id) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void update(Person person) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List<Person> getAll() 
	{
		List<Person> persons = new ArrayList<Person>();
		
	    Cursor cursor = _db.query(Persons.TABLE_PERSONS,
	            Persons.getAllColumns(), null, null, null, null, null);		
		
	    cursor.moveToFirst();
	    
	    while(!cursor.isAfterLast())
	    {
	    	Person _person = MapCursorToEntity(cursor);
	    	persons.add(_person);
	    	cursor.moveToNext();
	    }
	    
	    cursor.close();
	    
		return persons;
	}

	
}
