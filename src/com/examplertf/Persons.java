package com.examplertf;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class Persons {

	public static final String TABLE_PERSONS = "persons";
	public static final String COLUMN_ID = "_id";
	public static final String COLUMN_NAME = "name";
	public static final String COLUMN_LASTNAME = "last_name";

	
	public static String[] getAllColumns()
	{
		return new String[] {COLUMN_ID, COLUMN_NAME, COLUMN_LASTNAME};
	}
	
	private static final String DATABASE_CREATE = "create table " + TABLE_PERSONS 
			+ "(" + COLUMN_ID + " integer primary key autoincrement, " 
			+ COLUMN_NAME + 	" varchar(30) null, "
		    + COLUMN_LASTNAME + " varchar(30) null" +
		       ");";
	
	public static void onCreate(SQLiteDatabase db)
	{
		db.execSQL(DATABASE_CREATE);
	}
	
	public static void onUpdate(SQLiteDatabase db, int oldVersion, int newVersion)
	{
	    Log.w(Persons.class.getName(),
	            "Upgrading database from version " + oldVersion + " to "
	                + newVersion + ", which will destroy all old data");
	        db.execSQL("DROP TABLE IF EXISTS " + TABLE_PERSONS);
	        onCreate(db);		
	}		
	
}
