package com.examplertf;

import java.util.List;

import android.os.Bundle;
import android.app.Activity;
import android.graphics.Color;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

public class MainActivity extends Activity {

	private PersonDataSource personDataSource;
	private TableLayout _personTable;
	private Button _populateTable;
	private TableRow _row;
	private EditText _name, _lastName;
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
        _personTable = (TableLayout) findViewById(R.id.person_table);
        _populateTable = (Button) findViewById(R.id.btnAdd);
        _name = (EditText) findViewById(R.id.etName);
        _lastName = (EditText) findViewById(R.id.etLastName);

        // DI ??
        personDataSource = new PersonDataSource(getApplicationContext());
        
        if (_populateTable != null){
        	_populateTable.setOnClickListener(new View.OnClickListener() {

        		@Override
				public void onClick(View v) {
        			/// Of course.. more validations has to be made before inserting
		        	if( !"".equals(_name.getText().toString().trim()) && !"".equals(_lastName.getText().toString().trim()))
		        	{
		        		Person _person = new Person();
		        		_person.setName(_name.getText().toString());
		        		_person.setLastName(_lastName.getText().toString());
		        		
		        		insertPerson(_person);
		        		insertRealDataToTable();
		        	}
		        	
				}
			});
        }
        	
        if (_personTable != null)
        {
        	insertRealDataToTable();
        }
        
    }

    public void insertPerson(Person person)
    {
    	personDataSource.open();
    	personDataSource.addNew(person);
    	personDataSource.close();
    }
    
    public void populateDb()
    {
    	personDataSource.open();
    	
    	Person _person = new Person();
    	_person.setName("Andres");
    	_person.setLastName("Pineda");
    	
    	personDataSource.addNew(_person);
    	
    	_person = new Person();
    	_person.setName("Jose");
    	_person.setLastName("Perez");
    	
    	personDataSource.addNew(_person);
    	
    	_person = new Person();
    	_person.setName("Angelina");
    	_person.setLastName("Julie");
    	
    	personDataSource.addNew(_person);
    	
    	personDataSource.close();
    }
    
    public void insertRealDataToTable()
    {
    	personDataSource.open();
       List<Person> _persons =personDataSource.getAll();
       
       float scale = getResources().getDisplayMetrics().density;
       
       int leftPad = (int) (getResources().getDimension(R.dimen.tbl_row_left_pad)*scale + 0.5f);
       int topPad = (int) (getResources().getDimension(R.dimen.tbl_row_top_pad)*scale + 0.5f);
       int rightPad = (int) (getResources().getDimension(R.dimen.tbl_row_right_pad)*scale + 0.5f);
       int bottomPad = (int) (getResources().getDimension(R.dimen.tbl_row_bottom_pad)*scale + 0.5f);
       
       if (_persons != null){
    	   refreshDataTable();
    	   int i=0;
    	   for(Person _per : _persons){
    		   i++;
    		   _row = new TableRow(getApplicationContext());
    		   
    		   TextView tvNumber = new TextView(getApplicationContext());
    		   TextView tvId = new TextView(getApplicationContext());
    		   TextView tvName = new TextView(getApplicationContext());
    		   TextView tvLastName = new TextView(getApplicationContext());
    		   
    		   tvNumber.setPadding(leftPad, 
    				   				topPad,
    				   				rightPad,
    				   				bottomPad);
    		   
    		   tvId.setPadding(leftPad, 
		   				topPad,
		   				rightPad,
		   				bottomPad);
    		   
    		   tvName.setPadding(leftPad, 
		   				topPad,
		   				rightPad,
		   				bottomPad);
    		   
    		   tvLastName.setPadding(leftPad, 
		   				topPad,
		   				rightPad,
		   				bottomPad);
    		   
    		   tvNumber.setText( String.valueOf(i));
    		   tvId.setText( String.valueOf(_per.Id()));
    		   tvName.setText( _per.Name() );    		   
    		   tvLastName.setText( _per.LastName());
    		   
    		   _row.addView(tvNumber);
    		   _row.addView(tvId);
    		   _row.addView(tvName);
    		   _row.addView(tvLastName);
    		   
    		   _personTable.addView(_row);
    		   
               if (((i) % 2) == 0) {
                   _row.setBackgroundColor(Color.parseColor("#C3D3D3"));
               }
    	   }
       }
       
       personDataSource.close();
    }
    
    private void refreshDataTable(){
    	_personTable.removeViews(1, _personTable.getChildCount()-1);
    }

    
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }
    
}
