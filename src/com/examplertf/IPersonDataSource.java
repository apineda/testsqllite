package com.examplertf;

import java.util.List;

public interface IPersonDataSource {

	void open();
	void close();
	Person addNew(Person person);
	void delete(int id);
	void update(Person person);
	List<Person> getAll();
	
}
